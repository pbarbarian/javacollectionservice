package models;

import play.data.format.Formats;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity(name = "table_field")
@SequenceGenerator(name = "Token_generator", sequenceName = "table_field_seq", allocationSize = 1, initialValue = 1)
public class TableField extends Basis {

    @Constraints.Required
    private String label;

    @Constraints.Required
    @Formats.NonEmpty
    @Enumerated(EnumType.STRING)
    @Column(name = "table_field_type", length = 20, nullable = false)
    private TableFieldType tableFieldType;
    @Column(name = "table_field_required", columnDefinition = "boolean default false")
    private boolean tableFieldRequired;
    //if Autobox as Boolean then instead of false null will be created
    @Column(name = "table_field_active", columnDefinition = "boolean default false")
    private boolean tableFieldActive;
    @OneToMany(mappedBy = "tableField"
            , orphanRemoval = true, cascade = CascadeType.ALL
    )
    private Set<SelectOption> options = new HashSet<>();

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public TableFieldType getTableFieldType() {
        return tableFieldType;
    }

    public void setTableFieldType(TableFieldType tableFieldType) {
        this.tableFieldType = tableFieldType;
    }

    public boolean getTableFieldRequired() {
        return tableFieldRequired;
    }

    public void setTableFieldRequired(boolean tableFieldRequired) {
        this.tableFieldRequired = tableFieldRequired;
    }

    public boolean getTableFieldActive() {
        return tableFieldActive;
    }

    public void setTableFieldActive(boolean tableFieldActive) {
        this.tableFieldActive = tableFieldActive;
    }

    public Set<SelectOption> getOptions() {
        return options;
    }

    public void setOptions(Set<SelectOption> options) {
        this.options = options;
    }

    public String toString(){
        return this.getLabel();
    }
}
