package models;

import play.db.ebean.Model;

import javax.persistence.*;

@MappedSuperclass
public class Person extends Model{

    public Long id;

    public String name;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
