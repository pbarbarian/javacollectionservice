package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import dao.CrudOperations;
import models.DataField;
import models.Response;
import play.db.ebean.Model;
import play.db.ebean.Transactional;
import play.mvc.*;
import services.ResponseService;
import services.WebSocketService;
import views.html.response;
import views.html.responses;

import java.util.*;

import static play.libs.Json.toJson;

public class ResponseController extends Controller {

    private static Set<WebSocket.Out<String>> countResponsesStreamClients = new HashSet<>();
    private static Set<WebSocket.Out<String>> responsesStreamClients = new HashSet<>();

    public static Result getResponses() {
        return ok(responses.render("App is started!"));
    }

    public static Result getDataFieldsJson() {
        List<DataField> entities = new Model.Finder<>(Long.class, DataField.class).all();
//        List<TableField> entities = CrudOperations.all()
        return ok(toJson(entities));
    }

    public static Result deleteResponse(String id){
        Set<DataField> dfs = new Model.Finder<>(String.class, DataField.class)
                .where("response_id = '" + id + "'").findSet();
        CrudOperations.deleteAll(dfs);
        return ok();
    }

    public static Result getResponseForm(String id) {
        return (id.equals("new"))
                ? ok(response.render("Add Response", Long.toHexString(UUID.randomUUID().getMostSignificantBits())))
                : ok(response.render("Edit Response", id));
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result saveResponse(){
        JsonNode json = request().body().asJson();
        ResponseService.saveResponseFromJson(json);
        return ok();
    }

    @Transactional
    @BodyParser.Of(BodyParser.Json.class)
    public static Result getResponseJson(String id){
        List<DataField> entities = CrudOperations.getActiveFieldsResponseById(id);
        Response response = new Response(entities, String.valueOf(id));
        return ok(toJson(response));
    }

    @Transactional
    public static WebSocket<String> count() {
        String message = String.valueOf(ResponseService.getResponses(true).size());
        return new WebSocketService().wsHandle(message, countResponsesStreamClients);
    }

    @Transactional
    public static WebSocket<String> getResponsesStream(){
        boolean needActiveFields = true;
        String message = toJson(ResponseService.getResponses(needActiveFields)).toString();
        return new WebSocketService().wsHandle(message, responsesStreamClients);
    }
}
