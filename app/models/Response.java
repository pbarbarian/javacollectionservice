package models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class Response {
    private String uuid;
    private Map<TableField, String> map = new HashMap<>();

    //  Do not insert all the dataFields but just with the selected responseId
    public Response(List<DataField> dataFields){
        for (DataField df : dataFields){
            this.map.put(df.getTableField(), df.getValue());
        }
        this.uuid = Long.toHexString(UUID.randomUUID().getMostSignificantBits());
    }

    public Response(List<DataField> dataFields, String uuid){
        this(dataFields);
        if (uuid != null){
            this.uuid = uuid;
        }
    }

    public String getUuid() {
        return uuid;
    }

    public Map<TableField, String> getMap() {
        return map;
    }

    public void setMap(Map<TableField, String> map) {
        this.map = map;
    }
}
