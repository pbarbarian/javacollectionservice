package services;

import com.fasterxml.jackson.databind.JsonNode;
import dao.CrudOperations;
import models.SelectOption;
import models.TableField;
import models.TableFieldType;
import play.db.ebean.Model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by barbarian on 28.8.15.
 */
public class TableFieldService {
    public static void saveTableField(JsonNode json){
        Long id = json.get("id").asLong();
        TableField entity = new TableField();
        if (id != 0){
            entity = new Model.Finder<>(Long.class, TableField.class).byId(id);
        }
        //  Refreshing TableField Parameters
        entity.setLabel(json.get("label").asText());
        entity.setTableFieldRequired(json.get("tableFieldRequired").asBoolean());
        entity.setTableFieldActive(json.get("tableFieldActive").asBoolean());
        if (id == 0){
            //only if id == 0 cus we don't allow to change it due to data integrity
            TableFieldType tableFieldType = TableFieldType.valueOf(TableFieldType.class, json.get("tableFieldType").asText());
            entity.setTableFieldType(tableFieldType);
            //  We need entity.id for SelectOptions to link it
            entity.save();
        }
        //  Creating SelectOptions if we need it
        if (entity.getTableFieldType().equals(TableFieldType.COMBO_BOX)
                || entity.getTableFieldType().equals(TableFieldType.RADIO_BUTTON)){
            String opt = json.get("options").asText();
            Set<SelectOption> opts = new HashSet<>();
            for (String s : opt.split("\n")){
                SelectOption so = new SelectOption();
                so.setLabel(s);
                so.setName(s);
                so.setTableField(entity);
                opts.add(so);
            }
            entity = CrudOperations.updateAll(opts, entity);
        }
        entity.update();
    }

    public static List<TableField> getTableFields(boolean needActive){
        return CrudOperations.getActiveFields(needActive);
    }
}
