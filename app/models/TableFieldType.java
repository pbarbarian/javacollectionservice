package models;

public enum TableFieldType {
    SINGLE_LINE_TEXT ("Single line text"),
    MULTI_LINE_TEXT ("Multi line text"),
    RADIO_BUTTON ("Radio button"),
    CHECK_BOX ("Check box"),
    COMBO_BOX ("Combo box"),
    DATE ("Date");

    private final String name;
    private final Integer code;

    public String getName() {
        return name;
    }

    public Integer getCode() {
        return code;
    }

    private TableFieldType(String s) {
        name = s;
        code = this.ordinal();
    }

    public static TableFieldType forCode(int code) {
        for (TableFieldType e : TableFieldType.values()) {
            if (e.ordinal() == code)
                return e;
        }
        return null;
    }
}
