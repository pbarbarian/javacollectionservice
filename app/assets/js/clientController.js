function deleteObject(uri){
    var result = confirm("Really delete ?");
    if (!result){
        return;
    }
    $.ajax({
        url: uri,
        type: "POST"
    });
}

/**
 * Display the fields of the object on the Form page
 * @param id
 */
function displayObjectOnPage(url){
    $.ajax({
        url:  url,
        type: "GET",
        success: function(data, textStatus) {
            if (data.id !=null){
                $('#id').val(data.id);
                $('#label').val(data.label);
                $('#tableFieldType').val(data.tableFieldType);
                if (data.tableFieldRequired) {
                    var req = $('#tableFieldRequired');
                    req.prop('checked', true);
                    req.prop('disabled', 'disabled');
                    $('#tableFieldActive').prop('disabled', 'disabled');
                }
                if (data.tableFieldActive) {
                    $('#tableFieldActive').prop('checked', true);
                }
                var opt = "";
                for (var i = 0; i < data.options.length; i++){
                    opt += data.options[i].label + "\n";
                }
                $('#option').val(opt);
                //  showTextarea() function is defined in field.scala.html
                showTextarea();
            }
        }
    });
}

/**
 * Display fields on Response Create Page
 */
function displayTableFieldsAjax(url){
    $.ajax({
        url: url,
        dataType : "json",
        success: function(data, textStatus) {
            console.log(data);
            //  Generates Header of the table
            var out = "";
            var objectNumber = Object.keys(data).length;
            var collection = [];
            for (var number = 0; number <= objectNumber - 1; number++){
                collection[number] = data[number];
            }
            for (var i in collection){
                var nameAndId = "name='" + collection[i].label + "' id='" + collection[i].id;
                out += collection[i].label;
                if (collection[i].tableFieldType == 'SINGLE_LINE_TEXT'){
                    out += "<input type='text' " + nameAndId + "'/>";
                    out += "<br>";
                    continue;
                }
                if (collection[i].tableFieldType == 'MULTI_LINE_TEXT'){
                    out += "<textarea " + nameAndId + "'></textarea>";
                    out += "<br>";
                    continue;
                }
                if (collection[i].tableFieldType == 'COMBO_BOX'){
                    out += "<select " + nameAndId + "'>"
                    for (var ii in collection[i].options){
                        var opt = collection[i].options;
                        out += "<option value='" + opt[ii].label + "'>" + opt[ii].name + "</option>";
                    }
                    out += "</select>";
                    out += "<br>";
                    continue;
                }
                if (collection[i].tableFieldType == 'CHECK_BOX'){
                    out += "<input type=checkbox " + nameAndId + "'/>";
                    out += "<br>";
                    continue;
                }
                if (collection[i].tableFieldType == 'DATE'){
                    out += "<input type=date " + nameAndId + "'/>";
                    out += "<br>";
                    continue;
                }
                if (collection[i].tableFieldType == 'RADIO_BUTTON'){
                    for (var ii in collection[i].options){
                        var opt = collection[i].options;
                        out +="<br>";
                        out +="<input type=radio value='" + opt[ii].label + "' " + nameAndId + "'/>";
                        out +="<label for='" + opt[ii].id + "_" + ii + "'>" + opt[ii].label + "</label>";
                    }
                    out += "<br>";
                }
            }
            $("#tableFields").html(out);
        }
    });
}

/**
 * Display a table of all the fields
 * @param customUrl     url address to service that returns Data as JSON
 * @param elementUrl    prefix before link to an element (dynamic reference to element in web)
 */
function getDataAjax(url){
    $.ajax({
        url: url,
        dataType : "json",
        success: function(data, textStatus) {
            console.log(data);
            var result = displayAsTable(data, "/fields/", "data[i].id", "data[i]");
            $('#entities').html(result);
        }
    });
}

function getDataWebSocket(socketUri){
    $(function() {
        var WS = window['MozWebSocket'] ? MozWebSocket : WebSocket;
        var socket = new WS(socketUri);
        socket.onopen = function(){
            socket.send("Соединение открыто");
        };
        socket.onmessage = function(event) {
            var data = JSON.parse(event.data);
            var tableBody = displayAsTable(data, "/responses/", "data[i].uuid", "data[i].map");
            $('#entities').html(tableBody);
        };
    });
}

function showResponseOnPageAjax(url){
    $.ajax({
        url: url,
        dataType: "json",
        success: function (data, textStatus) {
            var fields = Object.keys(data.map);
            var el;
            for(var a in fields){
                el = document.getElementsByName(fields[a])[0];
                if (el.type != 'radio'){
                    el.value = data.map[fields[a]];
                }
                if (el.type != 'checkbox' && el.type != 'radio'){
                    continue;
                }
                if (el.type == 'checkbox' && el.value == 'true'){
                    el.checked = true;
                    continue;
                }
                if (el.type == 'radio'){
                    el = document.getElementsByName(fields[a]);
                    for (var aa in el){
                        if (el[aa].value == data.map[fields[a]]){
                            el[aa].checked = true;
                            break;
                        }
                    }
                }
            }
        }
    });
}

function saveResponseAjax(url){
    var responseId = document.getElementById('id').value;
    var fields = document.getElementById('submit').elements;
    var elements = [];
    for (var i = 0; i <= fields.length - 1; i++){
        if (fields[i].id != 'id'){
            var element = {tableFieldId: "", value: "", responseId: ""};
            element.tableFieldId = fields[i].id;
            element.value = fields[i].value;
            if (fields[i].type == 'checkbox'){
                element.value = fields[i].checked;
            }
            element.responseId = responseId;
            if (fields[i].type == 'radio' && fields[i].checked){
                elements[i] = element;
            }
            if (fields[i].type != 'radio'){
                elements[i] = element;
            }
        }
    }
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(elements),
        contentType: "application/json"
    });
}

function saveTableFieldAjax(url){
    var id = $('#id').val();
    var label = $('#label').val();
    var tableFieldType = $('#tableFieldType').val();
    var tableFieldRequired = $('#tableFieldRequired')[0].checked;
    var tableFieldActive = $('#tableFieldActive')[0].checked;
    var options = $('#option').val();
    var field = {
        id: id,
        label: label,
        tableFieldType: tableFieldType,
        tableFieldRequired: tableFieldRequired,
        tableFieldActive: tableFieldActive,
        options: options
    };
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(field),
        contentType: "application/json"
    });
}

