package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.TableField;
import play.db.ebean.Model;
import play.mvc.*;

import services.TableFieldService;
import views.html.*;

import static play.libs.Json.toJson;

public class Application extends Controller {

    public static Result getAllTableFields(){
        return ok(fields.render("Fields"));
    }

    public static Result getTableFieldForm(Long id) {
        return (id == 0) ? ok(field.render("Add Field", 0L)) : ok(field.render("Edit Field", id));
    }

    @BodyParser.Of(BodyParser.Json.class)
    public static Result saveTableField() {
        JsonNode json = request().body().asJson();
        TableFieldService.saveTableField(json);
        return ok();
    }

    public static Result getTableFieldJson(Long id) {
//        TableField tableField = CrudOperations.get(id);
        TableField tableField = new Model.Finder<>(Long.class, TableField.class).byId(id);
        return ok(toJson(tableField));
    }

    public static Result getTableFieldsJson(Boolean needActive) {
        return ok(toJson(TableFieldService.getTableFields(needActive)));
    }

    public static Result deleteTableField(Long id) {
        new Model.Finder<>(Long.class, TableField.class).byId(id).delete();
//        CrudOperations.delete(id);
        return redirect(routes.Application.getAllTableFields());
    }

    public static Result message(String message){
        return ok("<h1>" + message + "</h1>");
    }
}
