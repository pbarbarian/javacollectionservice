-- sudo -u postgres psql
-- DROP DATABASE if EXISTS test;
-- CREATE DATABASE test;
-- CREATE USER barbarian WITH password 'qwerty';
-- GRANT ALL privileges ON DATABASE test TO barbarian;
-- psql -h localhost test barbarian

--\l === show databases
--\c database === use database
--\dt === show tables
--\ds === show sequences
--\d table === show columns
--\d+table === describe table

--\du === show all user accounts
