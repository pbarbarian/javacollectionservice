package dao;

import com.avaje.ebean.Ebean;
import models.DataField;
import models.SelectOption;
import models.TableField;
import play.db.ebean.Model;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CrudOperations {

    public static Set<SelectOption> getAllByField(TableField tableField){
        Set<SelectOption> selectFromDB = new HashSet<>(new Model.Finder<>(Long.class, SelectOption.class).all());
        return selectFromDB.stream().filter(s -> s.getTableField().equals(tableField)).collect(Collectors.toSet());
    }

    //  todo: the options should be set to tableField out of here
    public static TableField updateAll(Set<SelectOption> formOptions, TableField tableField){
        Set<SelectOption> selectFromDB = getAllByField(tableField);
        Set<SelectOption> optionsForSave = new HashSet<>();

        // inserting new options in db
        if (formOptions.size() > 0){
            optionsForSave.addAll(formOptions.stream().filter(so -> !selectFromDB.contains(so))
                    .collect(Collectors.toList()));
            if (optionsForSave.size() > 0){
                CrudOperations.saveAll(optionsForSave);
                tableField.setOptions(optionsForSave);
            }
        }

        //  deleting unnecessary options from Db
        if (selectFromDB.size() > 0){
            Set<SelectOption> soForDelete = new HashSet<>();
            for (SelectOption so : selectFromDB) {
                if (formOptions.contains(so)) {
                    tableField.getOptions().add(so);
                } else {
                    soForDelete.add(so);
                }
            }
            if (soForDelete.size() > 0){
                CrudOperations.deleteAll(soForDelete);
            }
        }
        return tableField;
    }

    public static void saveAll(Collection entities){
        Ebean.save(entities);
    }

    public static void deleteAll(Collection entities){
        Ebean.delete(entities);
    }

    public static List<TableField> getActiveFields(boolean needActive){
        return needActive ? Ebean.find(TableField.class)
                .where()
                .eq("tableFieldActive", true)
                .findList()
                : Ebean.find(TableField.class).findList();
    }

    public static List<DataField> getDataFields(Boolean needActive){
        return needActive
                ? Ebean.find(DataField.class)
                    .where()
                    .eq("tableField.tableFieldActive", true)
                    .findList()
                : Ebean.find(DataField.class)
                    .findList();
    }

    public static List<DataField> getActiveFieldsResponseById(String id){
        return Ebean.find(DataField.class)
                .where()
                .eq("response_id", id)
                .eq("tableField.tableFieldActive", true)
                .findList();
    }

}
