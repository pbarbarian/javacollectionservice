/**
 * Need bootstrap to show glyphicons
 */

/**
 * Display JSON collection as table
 * @param data              JSON collection
 * @param elementUrl        prefix before link to an element (dynamic reference to element in web)
 * @param elementIdLink     string reference to id of any element
 * @param elementLink       string reference to any element in collection
 * @returns {string}        table in string format (return the value of the function into <table> tag
 */
function displayAsTable(data, elementUrl, elementIdLink, elementLink){
    //  Getting keys from the first element
    var firstElementLink = elementLink.replace("i", "0");
    var keys = getKeys(eval(firstElementLink));

    return generateTableHead(keys)
        + generateTableBody(data, keys, elementUrl, elementIdLink, elementLink);
}

/**
 * Get all the keys (neither id, no uuid) from JSON object
 * @param element       any element from which the fields will be get
 * @returns {Array}     array of keys
 */
function getKeys(element){
    var keys = [];
    var iterator = 0;
    for (var key in element){
        if (key != 'id' & key != 'uuid'){
            keys[iterator++] = key;
        }
    }
    return keys;
}

/**
 * Autoboxes keys into head tags of table
 * @param keys
 * @returns {string}
 */
function generateTableHead(keys){
    var out = "<thead><tr>";
    for (var key in keys){
        if (key != 'id' & keys[key] != 'uuid'){
            out += "<th>" + keys[key] + "</th>";
        }
    }
    out += "<th></th><th></th>";
    out += "</tr></thead>";
    return out;
}

/**
 * Generates Body of the table from @data with @keys
 * @param data              JSON of collection of objects
 * @param keys              array of fields in element of collection
 * @param elementUrl        prefix before link to an element (dynamic reference to element in web)
 * @param elementIdLink     path to the id value of the element
 * @param elementLink       path to the JSON object of the element
 * @returns {string}
 */
function generateTableBody(data, keys, elementUrl, elementIdLink, elementLink){
    var result = "<tbody>";
    for (var i in data){
        result += "<tr>";
        for (var key in keys){
            var elementId = eval(elementIdLink);
            var element = eval(elementLink);
            if (typeof element[keys[key]] == 'boolean'){
                result += element[keys[key]] == false
                    ? "<td><input type='checkbox' disabled></td>"
                    : "<td><input type='checkbox' checked disabled></td>";
                continue;
            }
            if (element[keys[key]] != null && typeof element[keys[key]] == 'object' ){
                result +="<td>";
                for (var a in element[keys[key]]){
                    result += element[keys[key]][a].label + "<br>";
                }
                result +="</td>";
                continue;
            }
            result += element[keys[key]] == null
                ? "<td style='color: #ce145d;'>" + "N/A" +"</td>"
                : "<td>" + element[keys[key]] + "</td>";

        }
        result += "<td><a href='" + elementUrl + elementId + "'><span class='glyphicon glyphicon-pencil'></span> edit</a></td>";
        result += "<td><a href='' onclick='deleteObject(\"" + elementUrl + elementId + "/delete\")'><span class='glyphicon glyphicon-trash'></span> delete</a></td>";
        result += "</tr>";
    }
    result += "</tbody>";
    return result;
}