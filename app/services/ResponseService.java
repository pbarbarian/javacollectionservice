package services;

import com.fasterxml.jackson.databind.JsonNode;
import dao.CrudOperations;
import models.DataField;
import models.Response;
import models.TableField;
import play.db.ebean.Model;
import play.db.ebean.Transactional;

import java.util.*;

/**
 * Created by barbarian on 28.8.15.
 */
public class ResponseService {
    @Transactional
    public static List<Response> getResponses(boolean needActive){
        //  Getting all the active fields even if no Response have it
        List<TableField> tableFields = CrudOperations.getActiveFields(needActive);
        //  Getting all the data in all Responses
        List<DataField> entities = CrudOperations.getDataFields(needActive);
        //  Creating collections of data and put it into maps which are Responses in fact
        Map<String, List<DataField>> dataFields = new HashMap<>();
        List<Response> responses = new ArrayList<>();
        for (DataField df : entities){
            if (!dataFields.containsKey(df.getResponseId())){
                dataFields.put(df.getResponseId(), new ArrayList<DataField>());
            }
            dataFields.get(df.getResponseId()).add(df);
        }
        //  Generating Responses from maps of data
        for (Map.Entry<String, List<DataField>> entry : dataFields.entrySet()){
            Response response = new Response(entry.getValue(), String.valueOf(entry.getValue().get(0).getResponseId()));
            //  Creates fields with null values which are active but not presented in Response
            tableFields.stream().filter(f -> !response.getMap().containsKey(f)).forEach(f -> {
                response.getMap().put(f, null);
            });
            responses.add(response);
        }
        return responses;
    }

    public static void saveResponseFromJson(JsonNode json){
        List<DataField> dataFields = new ArrayList<>();
        for (JsonNode temp : json) {
            if (!temp.isNull()) {
                Long tableFieldId = temp.get("tableFieldId").asLong();
                TableField tableField = new Model.Finder<>(Long.class, TableField.class).byId(tableFieldId);
                String value = temp.get("value").asText();
                String responseId = temp.get("responseId").asText();
                DataField dataField = new Model.Finder<>(Long.class, DataField.class)
                        .where("response_id = '" + responseId + "' and table_field_id = " + tableFieldId).findUnique();
                if (dataField == null){
                    dataFields.add(new DataField(tableField, value, responseId));
                } else {
//                    if the new and the old values are equals that we should not save it
                    if (!Objects.equals(dataField.getValue(), value)){
                        dataField.setValue(value);
                        dataFields.add(dataField);
                    }
                }
            }
        }
        CrudOperations.saveAll(dataFields);
    }
}
