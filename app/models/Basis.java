package models;

import play.db.ebean.Model;

import javax.persistence.*;

@MappedSuperclass
//@SequenceGenerator(name = "Token_generator", sequenceName = "test_sequence", allocationSize = 1, initialValue = 1)
public abstract class Basis extends Model{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "Token_generator")
    public Long id;

    public Long getId() {
        return id;
    }
}
