package models;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.util.Objects;

@Entity(name = "select_option")
@SequenceGenerator(name = "Token_generator", sequenceName = "select_option_seq", allocationSize = 1, initialValue = 1)
public class SelectOption extends Basis {

    private String name;
    private String label;
    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "table_field_id")
    private TableField tableField;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public TableField getTableField() {
        return tableField;
    }

    public void setTableField(TableField tableField) {
        this.tableField = tableField;
    }

    public int hashCode(){
        return  this.label.hashCode();
    }

    public boolean equals(Object obj) {
        SelectOption entity2 = (SelectOption) obj;
        return (Objects.equals(this.getLabel(), entity2.getLabel()));
    }
}
