package services;

import play.Logger;
import play.mvc.WebSocket;

import java.util.Set;

/**
 * Created by barbarian on 6.9.15.
 */
public class WebSocketService {

    public static WebSocket<String> wsHandle(String message, Set<WebSocket.Out<String>> wsClients){
        return new WebSocket<String>() {
            @Override
            public void onReady(In<String> in, Out<String> out) {
                if (wsClients.add(out)) {
                    Logger.info("Response Connection " + out.toString() + " is established.");
                }
                in.onMessage(s -> {
                    //  Notifies all the clients about the event
                    for (WebSocket.Out<String> client : wsClients){
                        client.write(message);
                    }
                });
                in.onClose(() -> {
                    Logger.info("Response Connection " + out.toString() + " is closed");
                    wsClients.remove(out);
                });
            }
        };
    }
}
