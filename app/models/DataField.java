package models;

import play.data.validation.Constraints;

import javax.persistence.*;

@Entity(name = "data_field")
@SequenceGenerator(name = "Token_generator", sequenceName = "data_field_seq", allocationSize = 1, initialValue = 1)
public class DataField extends Basis {

    private String value;
    @ManyToOne(optional = false)
    @JoinColumn(name = "table_field_id")
    private TableField tableField;
    @Constraints.Required
    @Column(name = "response_id")
    private String responseId;

    public DataField(){}

    public DataField(TableField tableField, String value, String responseId){
        this.tableField = tableField;
        this.value = value;
        this.responseId = responseId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public TableField getTableField() {
        return tableField;
    }

    public void setTableField(TableField tableField) {
        this.tableField = tableField;
    }

    public String getResponseId() {
        return responseId;
    }

    public void setResponseId(String responseId) {
        this.responseId = responseId;
    }
}
