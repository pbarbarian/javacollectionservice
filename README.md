# Java Collection Service #
**The product is aimed to allow User to create & operate tables via web interface**

*Now it allows creating and editing fields and responses only*

**Technologies Stack:**

* Java 8
* Play Framework 2.4 (+WebSockets)
* Hibernate
* PostgreSQL
* HTML/CSS/JS + jQuery + AJAX
* Bootstrap (poor)

[> Task file here <](https://drive.google.com/open?id=0B9d3CP1rzTTPUlowblJWQ0wwMFNHTktpczBNckt1VmpqTjhz)

INSTALLING
=====================================

# 1. Install Play Framework #

Look through https://www.playframework.com/download to find the latest version, download it and extract to your disk
where you have read/write access.

## For Windows Family ##
Copy the path to the root directory of the Play Framework and add it to the PATH Variables in
Computer->Properties->Advanced System Settings->Tab “Advanced”->Button “Environment Variables”.
(Select the PATH variable and add the root path of Play Framework to the end)

## For Linux Family ##
Set the path to the root folder of your version of the Play Framework.
export PATH=$PATH:/usr/local/lib/play-2.2.6/
Then make the path permanent: add the path to it in /etc/environment and reboot for the changes to take effect.

Run the command prompt and try “play help”. If everything is OK, you’ll see the greeting "Welcome to play <version>!"

# 2. Install PostgreSQL DBMS #

## For Linux Family ##

Install DBMS

```
#!console

sudo apt-get update
sudo apt-get install postgresql postgresql-contrib
```


Run postgres

```
#!console

sudo -u postgres psql
```

Set the password to the user postgres

```
#!console

alter user postgres password 'password'
```


Either after the installing PostgreSQL on your machine, you should edit the configuration of the DB connection according your parameters:
edit properties in **conf/application.conf** with pattern db.default.*

# 3. Install and config the application#

3.1 Download the application


```
#!console

git clone https://pbarbarian@bitbucket.org/pbarbarian/javacollectionservice.git
```

3.2 Config the file /conf/application.conf according to your DB account, you need lines

```
#!console

db.default.url
db.default.user
db.default.password
```
3.3 Open project root folder in your console and run the app with "play run"

# Application URLs #


```
#!console

domainname.com/fields    :  List of all the fields
domainname.com/responses :  List of all the responses
```
